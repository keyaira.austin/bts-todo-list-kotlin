package com.example.bts_todo_list_kotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ListView
import android.util.Log
import android.widget.ArrayAdapter


class MainActivity : AppCompatActivity() {

    private var TAG = "ToDoList"
    private lateinit var listView: ListView
    private lateinit var dataList: List<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //find list view
        listView = findViewById(R.id.list_view)

        //create list data
        dataList = listOf("The five boxing wizards jump quickly.")
        dataList.forEach{
                item -> Log.d(TAG,item)
        }


        //hook list data up to our list using array adapter
        val listAdapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, dataList)
        listView.adapter = listAdapter


    }
}